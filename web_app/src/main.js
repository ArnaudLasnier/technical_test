import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false;
Vue.prototype.$_apiURL = process.env.VUE_APP_REST_API_URL;

new Vue({
    render: h => h(App),
}).$mount('#app');
