\echo ''
\echo '# Beginning transaction'
\echo '======================='
begin;

\echo ''
\echo '# Initalizing extensions'
\echo '========================'
\include_relative extensions/uuid_ossp.sql
\include_relative extensions/tablefunc.sql

\echo ''
\echo '# Initializing datastore'
\echo '========================'
create schema datastore;
\include_relative datastore/countries.sql
\include_relative datastore/prices.sql

\echo ''
\echo '# Initializing the API'
\echo '======================'
create schema api;
\include_relative api/price_groups.sql
\include_relative api/country_codes.sql

\echo ''
\echo '# Committing transaction'
\echo '========================'
commit;
