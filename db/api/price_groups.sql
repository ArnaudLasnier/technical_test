\echo ''
\echo '## Price Groups View'
\echo '--------------------'

create view api.price_groups as
    select
        cast(ct.datetime as date) as date,
        date_part('hour', ct.datetime) as hour,
        ct.be_price,
        ct.ch_price,
        ct.cz_price,
        ct.de_at_price,
        ct.dk1_price,
        ct.dk2_price,
        ct.es_price,
        ct.fr_price,
        ct.nl_price
    from tablefunc.crosstab($$
        select
            p.datetime as datetime,
            c.code as country_code,
            p.amount as price
        from
            datastore.prices as p
            left outer join datastore.countries as c
                using (country_id)
        order by datetime asc, country_code asc
    $$) as ct(
        datetime timestamp,
        be_price real,
        ch_price real,
        cz_price real,
        de_at_price real,
        dk1_price real,
        dk2_price real,
        es_price real,
        fr_price real,
        nl_price real
    );
