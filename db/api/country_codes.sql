\echo ''
\echo '## Country Codes View'

create view api.country_codes as
    select code
    from datastore.countries
    order by code asc;
