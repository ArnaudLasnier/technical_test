\echo ''
\echo '## UUID OSSP Extension'
\echo '----------------------'

create schema uuid_ossp;
create extension "uuid-ossp" with schema uuid_ossp;
