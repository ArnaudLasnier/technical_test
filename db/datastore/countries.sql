\echo ''
\echo '## Countries Table'
\echo '------------------'

create table datastore.countries (
    country_id uuid primary key default uuid_ossp.uuid_generate_v4(),
    code varchar(10) unique not null,
    name text unique not null
);

insert into datastore.countries(code, name) values
    ('BE', 'Belgium'),
    ('CH', 'Switzerland'),
    ('CZ', 'Czech Republic'),
    ('DE_AT', 'Germany Austria'),
    ('DK1', 'Denmark1'),
    ('DK2', 'Denmark2'),
    ('ES', 'Spain'),
    ('FR', 'France'),
    ('NL', 'Netherland');
