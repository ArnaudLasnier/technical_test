\echo ''
\echo '## Prices Table'
\echo '---------------'

create table datastore.prices (
    price_id uuid primary key default uuid_ossp.uuid_generate_v4(),
    country_id uuid not null references datastore.countries,
    amount real not null,
    datetime timestamp not null
);
