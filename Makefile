up: ## Create and start the backend containers (no logs).
	docker-compose up -d

up_dev: ## Create and start the backend containers, with logs.
	docker-compose up

down: ## Stop and remove the backend containers.
	docker-compose down --remove-orphans

psql: ## Connect to the containerized development database through psql.
	docker-compose exec -u postgres db psql

web: ## Start the frontend development server.
	cd ./web_app && npm run serve

.PHONY: help
help: ## Show the documentation of the available commands (same as make).
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
