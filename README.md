# Technical Test

> Arnaud Lasnier \
> Thursday 10 March 2020

This repository is a technical test I had for a software engineering internship.

## Instructions

### Objectives

### Problem formulation

[EPEX SPOT SE (the European Power Exchange)](https://www.epexspot.com/en/company-info/epex_spot_in_the_power_market) is an exchange for power spot trading in Germany, France, the United Kingdom, the Netherlands, Belgium, Austria, Switzerland and Luxembourg.

Every day at 12h, the day ahead market closes and delivers a price for each European country. The CSV file contains an extract of the market results from 04/12/2017 to 10/12/2017 for the following countries:

Code  | Country
------|--------
BE    | Belgium
CH    | Switzerland
CZ    | Czech Republic
DE_AT | Germany-Austria
DK1   | Denmark1
DK2   | Denmark2
ES    | Spain
FR    | France
NL    | Netherland

### Requirements

- Your clients want a web interface to display prices results for any date range (one or several continuous dates) and any countries (one or more).

- The data should be provided by a RESTful service to get the price results from the CSV file provided.

## Running the proposed implementation

This project has the following system requirements:

- Node.js (developed on v13.8.0, but v8.0.0+ should be enough),
- Docker and Docker Compose.

Launch an operational backend (the REST API is available on `http://localhost:3001`):

``` bash
make up
```

Launch the frontend web server (the development web server is available on `http://localhost:8080`):

``` bash
cd web_app
npm install
npm run serve
```

Once you are done, stop the development web server (Ctrl+C) and then (in the project's folder):

``` bash
make down
```

## What I find interesting

### Schema Driven Development

This implementation borrows ideas intensively from:

- [Derek Sivers](https://sivers.org/pg)
- [Dimitri Fontaine](https://theartofpostgresql.com/)
- [Joe Nelson, Ruslan Talpă & Co.](https://www.freecodecamp.org/news/stop-calling-postgrest-magic-8f3e1d5e5dd1/)
- [Caleb Meredith](https://www.graphile.org/postgraphile/evaluating/#schema-driven-apis)
- [David Wheeler](https://pgtap.org/)

Simply put, they are all pretty much saying the same thing: the question is not
whether or not you should put business logic inside the database but *how much*
business logic you should put inside of it.

Perhaps the most exhaustive conference (in French!) on the subject was given by
[Francois-Guillaume Ribreau](https://www.youtube.com/watch?v=IskE3m3VjRY).

With projects like [PostgREST](http://postgrest.org/en/v6.0/) or
[Postgraphile](https://www.graphile.org/postgraphile/) that do a database
schema introspection and resolve the graph made by PK/FK associations, you can
enable a very powerful “schema driven development”.

I am a big fan of this approach and naturally found myself writting a lot more
SQL and PL/pgSQL than before.

In this implementation, I tried to clearly distinguish between data modelling
concerns (`./db/datastore`) and API (`./db/api`). The provided CSV file was in
fact a pivoted version of the data, so I unpivoted it using pandas and solved
the date/time issues to seed it into the `prices` table.

However, since the API should provide a pivoted version of the data, I
repivoted it using the `tablefunc` PostgreSQL extension
(`./db/api/price_groups.sql`). Now the HTTP endpoint `/price_groups` gives us
what we want.

### Containers, containers, containers!

I am a big fan of containers. So I tried to make the most seamless installation
possible. If you have Docker (and docker-compose) installed, just run `make up`
and you are good to go: it takes care of fetching images, installing Python
packages and starts the required containers for the backend.

Also, I leveraged the initialization method provided by the official PostgreSQL
image: the `db` container executes the `initdb.sql` file which in turn includes
(through psql meta-commands) the appropriate DDL/DML statements.
