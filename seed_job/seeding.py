"""Seeding

The responsability of this module is to expose the `insert_data_into_db`
function that takes a pandas `DataFrame` as its input and insert its data into
the given table.

At this point, the data contained in the `DataFrame` is considered clean: the
`insert_data_in_db` function will not try to reformat it.
"""

import pandas as pd
import sqlalchemy as sqla

def insert_prices_into_db(
    df: pd.DataFrame,
    engine: sqla.engine.Engine,
    schema: str,
    table: str
):
    """This function inserts the data contained in its given pandas `DataFrame`
    into the given database table.

    It does not, however, perform any data cleaning / formating.
    """

    df.to_sql(
        name=table,
        con=engine,
        schema=schema,
        if_exists="append",
        index=False,
        method="multi"
    )
