"""Seed Job

This script inserts the prices contained in a CSV file into the database. The
location of both the CSV file and the database table can be specified as
environment variables.

It relies on two modules for two separate steps: (1) the `preprocessing` module
that appropriately formats the data and (2) the `seeding` module that actually
inserts the prices into the database.
"""

import os
import sqlalchemy as sqla
import pandas as pd

from preprocessing import get_insertable_prices
from seeding import insert_prices_into_db

CSV_PATH = os.getenv("CSV_PATH")
POSTGRES_URI = os.getenv("POSTGRES_URI")

engine: sqla.engine.Engine = sqla.create_engine(POSTGRES_URI)

# Preprocessing
insertable_prices = get_insertable_prices(
    csv_path=CSV_PATH,
    db_engine=engine,
)

# Seeding
insert_prices_into_db(
    df=insertable_prices,
    engine=engine,
    schema="datastore",
    table="prices",
)

engine.dispose()
