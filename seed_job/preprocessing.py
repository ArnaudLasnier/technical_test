"""Preprocessing

The responsability of this module is to expose the `get_insertable_prices`
function that returns a pandas `DataFrame` whose data can easily be inserted
into the database.
"""

import pandas as pd
import datetime as dt
import sqlalchemy as sqla
from typing import Tuple

def _parse_date_string_without_hour(date_string_without_hour: str) -> Tuple[int, int, int]:
    year = int(date_string_without_hour[0:4])
    month = int(date_string_without_hour[4:6])
    day = int(date_string_without_hour[6:8])

    return year, month, day


def _parse_hour_string(hour_string: str) -> int:
    hours = int(hour_string[1:])

    return hours


def _parse_datetime_string(datetime_string: str) -> dt.datetime:
    date_string_without_hour, hour_string = datetime_string.split(" - ")
    year, month, day = _parse_date_string_without_hour(date_string_without_hour)
    hours = _parse_hour_string(hour_string)

    return dt.datetime(year, month, day) + dt.timedelta(hours=hours)


def _rewrite_datetime_string_as_iso_format(df: pd.DataFrame) -> pd.DataFrame:
    df["Dates"] = df["Dates"].apply(
        lambda datetime_string: _parse_datetime_string(datetime_string).isoformat()
    )

    return df


def _unpivot(pivoted_df: pd.DataFrame) -> pd.DataFrame:
    unpivoted_df = pivoted_df.melt(id_vars=["Dates"])
    unpivoted_df.rename(columns={
        "variable": "country_code",
        "value": "amount"
    }, inplace=True)
    return unpivoted_df


def _rename_dates_column(df: pd.DataFrame) -> pd.DataFrame:
    df.rename(columns={
        "Dates": "datetime",
    }, inplace=True)

    return df


def _get_prices(path: str) -> pd.DataFrame:
    """This function returns a pandas `DataFrame` modelling prices with three
    columns: `datetime`, `country_code` and `amount`. It gets its data from the
    CSV file (specified with a path string).
    """

    return pd.read_csv(
        path,
        sep=",",
        header=0,
    ).pipe(
        _rewrite_datetime_string_as_iso_format
    ).pipe(
        _unpivot
    ).pipe(
        _rename_dates_column
    )


def _get_countries(engine: sqla.engine.Engine) -> pd.DataFrame:
    """This function returns a pandas `DataFrame` modelling countries with two
    columns: `country_code` and `country_id`. It gets its data from the
    database (specified with a SQLAlchemy `Engine`).
    """

    query = """
        select
            code,
            country_id
        from datastore.countries
        order by code
    """

    return pd.read_sql_query(
        query,
        con=engine
    ).rename(columns={
        "code": "country_code"
    })

def get_insertable_prices(csv_path: str, db_engine: sqla.engine.Engine):
    return _get_prices(csv_path).merge(
        _get_countries(db_engine),
        on="country_code",
    ).drop(
        columns=["country_code"],
    )
